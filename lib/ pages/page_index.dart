import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: const Text(
          '내 사랑 구르미',
          style: TextStyle(color: Colors.white),
        ),
          centerTitle:true,
        elevation: 0.0,
        leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              print('메뉴버튼을 클릭했습니다.');
            }),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/gu1.jpg'),
            const Column(
              children: [
                Text('구르미를 소개합니다.',
                    style: TextStyle(
                        fontFamily: "Yeongdeok Sea",
                        color: Colors.blueGrey,
                        fontSize: 30
                    ),
                    textAlign: TextAlign.center),
                Text('겁쟁이지만 엄청난...에너자이저랍니다^^',
                    style: TextStyle(
                        fontFamily: "Yeongdeok Sea",
                        color: Colors.blueAccent,
                        fontSize: 20)),
              ],
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/gu2.jpeg',
                        width: 200, height: 200, fit: BoxFit.cover),
                    Image.asset('assets/gu3.jpeg',
                        width: 200, height: 200, fit: BoxFit.cover),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/gu4.jpeg',
                        width: 200, height: 200, fit: BoxFit.cover),
                    Image.asset('assets/gu.jpeg',
                        width: 200, height: 200, fit: BoxFit.cover),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
